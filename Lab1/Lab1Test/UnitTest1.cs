using Lab1.Controllers;
using Lab1.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Lab1Test
{
    [TestClass]
    public class UnitTest1
    {

        [TestInitialize]
        public void Initialize()
        {
            DealershipMgr.AddDealership(new Dealership { Name = "Joe's Auto", Email = "Joe@localhost" });
            DealershipMgr.AddDealership(new Dealership { Name = "Bob's Bargain Auto", Email = "Bob@localhost" });
        }

        [TestCleanup]
        public void CleanUp()
        {
            DealershipMgr.DropAllDealerships();
        }

        [TestMethod]
        public void DealershipApiGetAllTest()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);

            Assert.AreEqual("Bob's Bargain Auto", dealerships[1].Name);
            Assert.AreEqual("Bob@localhost", dealerships[1].Email);

        }

        [TestMethod]
        public void DealershipApiGetSpecificTest()
        {
            Dealership dealership = DealershipMgr.GetDealership(1);
            Assert.AreEqual("Joe's Auto", dealership.Name);
            Assert.AreEqual("Joe@localhost", dealership.Email);
        }

        [TestMethod]
        public void DealershipApiGetSpecificTestInvalidId()
        {
            Dealership dealership = DealershipMgr.GetDealership(5);
            Assert.IsNull(dealership);
        }

        [TestMethod]
        public void DealershipInsertTest()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.IsTrue(DealershipMgr.AddDealership(new Dealership { Name = "Colin's Cars", Email = "Colin@localhost" }));

            Assert.AreEqual(3, dealerships.Count);
            Assert.AreEqual("Colin's Cars", dealerships[2].Name);
            Assert.AreEqual("Colin@localhost", dealerships[2].Email);
        }

        [TestMethod]
        public void DealershipInsertTestMissingEmail()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.IsFalse(DealershipMgr.AddDealership(new Dealership { Name = "Colin's Cars" }));

            Assert.AreEqual(2, dealerships.Count);
        }

        [TestMethod]
        public void DealershipInsertTestMissingName()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.IsFalse(DealershipMgr.AddDealership(new Dealership { Email = "Colin@localhost" }));

            Assert.AreEqual(2, dealerships.Count);
        }

        [TestMethod]
        public void DealershipUpdateTest()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);

            Assert.IsTrue(DealershipMgr.UpdateDealership(1, new Dealership { Name = "Joe's Cars", Email = "JoesCars@localhost" }));

            dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Cars", dealerships[0].Name);
            Assert.AreEqual("JoesCars@localhost", dealerships[0].Email);
        }

        [TestMethod]
        public void DealershipUpdateTestMissingName()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);

            Assert.IsFalse(DealershipMgr.UpdateDealership(1, new Dealership { Email = "JoesCars@localhost" }));

            dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);
        }

        [TestMethod]
        public void DealershipUpdateTestMissingEmail()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);

            Assert.IsFalse(DealershipMgr.UpdateDealership(1, new Dealership { Name = "Joseph" }));

            dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
            Assert.AreEqual("Joe's Auto", dealerships[0].Name);
            Assert.AreEqual("Joe@localhost", dealerships[0].Email);
        }

        [TestMethod]
        public void DealershipDeleteTest()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.IsTrue(DealershipMgr.DeleteDealership(2));

            dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(1, dealerships.Count);
        }

        [TestMethod]
        public void DealershipDeleteTestInvalidId()
        {
            List<Dealership> dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);

            Assert.IsFalse(DealershipMgr.DeleteDealership(20));

            dealerships = DealershipMgr.GetDealerships();
            Assert.AreEqual(2, dealerships.Count);
        }


    }
}
