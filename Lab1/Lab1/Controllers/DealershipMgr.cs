﻿// I, Colin Kent-Shepherd, student number 000348270, certify that this material is my
// original work. No other person's work has been used without due
// acknowledgement and I have not made my work available to anyone else.

using Lab1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab1.Controllers
{
    public static class DealershipMgr
    {
        static List<Dealership> dealerships = new List<Dealership>();

        public static bool IsDealerValid (Dealership dealer)
        {
            return (dealer.Name != null && dealer.Email != null);
        }

        public static List<Dealership> GetDealerships()
        {
            return dealerships;
        }

        public static Dealership GetDealership(int? id)
        {
            if (id == null || dealerships.Max(dealership => dealership.Id) < id)
            {
                return null;
            }

            return dealerships.Find(dealership => dealership.Id == id);
        }

        public static bool AddDealership(Dealership dealer)
        {
            if (!IsDealerValid(dealer))
            {
                return false;
            }
            dealer.Id = dealerships.Count > 0 ? (dealerships.Max(dealership => dealership.Id)) + 1 : 1;
            dealerships.Add(dealer);
            return true;
        }

        public static bool UpdateDealership(int id, Dealership dealer)
        {
            if (!IsDealerValid(dealer) || !DealershipExists(id))
            {
                return false;
            }
            dealer.Id = id;
            dealerships[dealerships.FindIndex(dealership => dealership.Id == id)] = dealer;
            return true;
        }

        public static bool DeleteDealership(int id)
        {
            return dealerships.Remove(dealerships.Find(dealership => dealership.Id == id));
        }

        public static bool DealershipExists(int id)
        {
            return dealerships.Any(dealership => dealership.Id == id);
        }

        public static void DropAllDealerships()
        {
            dealerships = new List<Dealership>();
        }
    }
}