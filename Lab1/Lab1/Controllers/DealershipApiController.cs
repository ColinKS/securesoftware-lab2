﻿// I, Colin Kent-Shepherd, student number 000348270, certify that this material is my
// original work. No other person's work has been used without due
// acknowledgement and I have not made my work available to anyone else.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab1.Controllers
{
    [Produces("application/json")]
    [Route("api/DealershipApi")]
    public class DealershipApiController : Controller
    {

        // GET: api/DealershipApi
        [HttpGet]
        public IEnumerable<Dealership> Get()
        {
            return DealershipMgr.GetDealerships();
        }

        // GET: api/DealershipApi/5
        [HttpGet("{id}", Name = "Get")]
        public Dealership Get(int id)
        {
            return DealershipMgr.GetDealership(id);
        }

        // POST: api/DealershipApi
        [HttpPost]
        public void Post([FromBody] Dealership dealer) 
        {
            DealershipMgr.AddDealership(dealer);
        }

        // PUT: api/DealershipApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Dealership dealer)
        {
            DealershipMgr.UpdateDealership(id, dealer);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DealershipMgr.DeleteDealership(id);
        }
    }
}