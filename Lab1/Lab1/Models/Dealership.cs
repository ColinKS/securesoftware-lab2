﻿// I, Colin Kent-Shepherd, student number 000348270, certify that this material is my
// original work. No other person's work has been used without due
// acknowledgement and I have not made my work available to anyone else.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

//Model - Add the Dealership entity with the following mandatory fields: ID, Name and Email.  
//It should also have an optional PhoneNumber field.    
//Use suitable annotation attributes for display and validation purposes.

namespace Lab1.Models
{
    public class Dealership
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        [Required(ErrorMessage = "Email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public String Email { get; set; }

    }
}
