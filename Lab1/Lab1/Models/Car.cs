﻿// I, Colin Kent-Shepherd, student number 000348270, certify that this material is my
// original work. No other person's work has been used without due
// acknowledgement and I have not made my work available to anyone else.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


//Model - Add a Car entity with the following mandatory fields: ID, Make, Model, Year and VIN.
//It should also have optional Color and DealershipID fields.Use suitable annotation attributes for display and validation purposes.
//A navigation property property for Dealership should NOT be used as this information is stored elsewhere and the DealershipID field is only there for future
namespace Lab1.Models
{
    public class Car
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public String Make { get; set; }

        [Required]
        public String Model { get; set; }

        [Required]
        [Range(1995, 2020)]
        public int Year { get; set; }

        [Required]
        [StringLength(17, MinimumLength = 17)]
        public String VIN { get; set; }

        public String Color { get; set; }

        public int DealershipID { get; set; }
    }
}
