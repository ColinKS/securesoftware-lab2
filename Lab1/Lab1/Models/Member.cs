﻿// I, Colin Kent-Shepherd, student number 000348270, certify that this material is my
// original work. No other person's work has been used without due
// acknowledgement and I have not made my work available to anyone else.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab1.Models
{
    //Model - Add a Member class with the following fields(FirstName, LastName, UserName, Email, Company, Position and BirthDate) using 
    //suitable annotation attributes for display and validation purposes.Assume that all fields are mandatory except Company, Position and BirthDate.
    public class Member
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public String FirstName { get; set; }

        [Required]
        public String LastName { get; set; }

        [Required]
        public String UserName { get; set; }

        [Required(ErrorMessage = "Email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public String Email { get; set; }

        public String Company { get; set; }

        [Required]
        public String Position { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
    }
}
